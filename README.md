# social-housing

dashboard project to analyze social housing in France

## Le sujet

outil d'analyse et de mesure pour déterminer si la construction de logement sociaux en France ces dernières années contribue ou pas à la mixité sociale.

travailler sur 2 sources :

- Les données carroyées de l'insee qui donne par carreaux de 200 mètre des indicateurs sur le niveau de vie des habitants

- Les données du parc locatif social géolocalisé à jour du 1er janvier 2019 qui permet de localiser les logements sociaux

## Les sources

### Les données carroyées Insee

url : https://www.insee.fr/fr/statistiques/4176290?sommaire=4176305#consulter
Les données se trouvent dans `extdata/Filosofi2015_carreaux_200m_metropole_csv`

#### Présentation des données

Présentation des données carroyées à 200m

La grille de niveau 200 mètres correspond à un pavage du territoire français par des carreaux de 200 mètres de côté. Sur certains carreaux, le nombre de ménages fiscaux peut être inférieur à 11, seuil de confidentialité pour la source fiscale. Dans ce cas, la donnée qui est présente dans le fichier est imputée. L’utilisateur est informé dans ce cas par la présence d’une indicatrice I_est_cr qui est égale à 1 en cas d’imputation.

Ces données proviennent du dispositif sur les revenus localisés sociaux et fiscaux (FiLoSoFi).

La base contient 26 variables sur la structure par âge des individus, sur les caractéristiques des ménages et des logements et sur les revenus perçus au cours de l’année 2015. Le champ géographique est constitué de la France métropolitaine, de la Martinique et de La Réunion.

#### Documentation

Le dictionnaire des données se trouve [ici](https://www.insee.fr/fr/statistiques/4176290?sommaire=4176305#dictionnaire)
# données actualisées au 1er janvier 2023
https://www.statistiques.developpement-durable.gouv.fr/catalogue?page=dataset&datasetId=6390f7cb84f0679b04942fc2

### Le parc de logement sociaux géolocalisé

url :https://www.data.gouv.fr/fr/datasets/repertoire-des-logements-locatifs-des-bailleurs-sociaux/#_
Les données se trouvent dans `extdata/rpls_geoloc_2019`

#### Présentation des données

Le répertoire des logements locatifs des bailleurs sociaux a pour objectif de dresser l’état
global du parc de logements locatifs de ces bailleurs sociaux au 1er janvier d’une année. Mis en place au 1er janvier 2011, il est alimenté par les informations transmises par les bailleurs sociaux.

Cette mise à disposition répond aux attentes du décret n°2009-1485 du 2 décembre 2009 relatif au répertoire des logements locatifs des bailleurs sociaux. Il liste les informations contenues en lien avec l'article R411-4 : "Toute personne qui en fait la demande auprès du service statistique ministériel du logement peut obtenir communication, par voie électronique et gratuitement, des informations mentionnées aux d, e, f, g et j de l'article R411-3, pour tout logement locatif figurant dans le répertoire, à l'exclusion des logements des sociétés d'économie mixte qui ne donnent pas lieu au versement de la cotisation prévue à l'article L. 452-4".

# techno
R shiny app deployed with docker