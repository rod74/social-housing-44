# https://juanitorduz.github.io/dockerize-a-shinyapp/
# modernisé avec https://blog.sellorm.com/2021/04/25/shiny-app-in-docker/
# Utiliser l'image shiny de base du projet rocker pour ne pas avoir besoin d'en créer une.
# Cette image contient Shiny Server, un outil d'exécution shiny open source de RStudio.
# Ne pas utiliser la "dernière" version de l'image, car pas de garantie des versions de R
# et d'autres dépendances si dernière version,
# l'épingler à une version spécifique de R à la place. (ou voir avec renv)
#
# git clone https://gitlab.com/rod74/social-housing.git .
# docker build -t social-housing .
# docker run --rm -p 8080:8080 social-housing
#
# Ex. https://social-housing.shuffledata.net

# https://docs.rstudio.com/shiny-server/

# Build an image starting with https://hub.docker.com/u/rocker/

# FROM rocker/shiny:latest
FROM rocker/shiny:4.0.5

# system libraries of general use
RUN sudo apt-get update -qq && apt-get -y --no-install-recommends install \
libxml2-dev \
libcairo2-dev \
libsqlite3-dev \
libpq-dev \
libssh2-1-dev \
unixodbc-dev \
libcurl4-openssl-dev \
libssl-dev \
libv8-dev \
libprotobuf-dev \
protobuf-compiler \
libjq-dev \
libudunits2-dev \
gdal-bin \
proj-bin \
libgdal-dev \
libproj-dev \
libssl-dev \
libsasl2-dev

## update system libraries
      # RUN apt-get update && \
      # apt-get upgrade -y && \
      # apt-get clean

# RUN R -e 'install.packages(c(\
#               "dplyr", \
#               "shiny", \
#               "shinydashboard", \
#               "ggplot2" \
#             ), \
#             repos="https://packagemanager.rstudio.com/cran/__linux__/focal/2021-04-23"\
#           )'

RUN R -e 'install.packages(c("dplyr", "sf", "stringr", "ggplot2", "mapview", "tmap", "cartography", "leaflet", "shinyWidgets", "shinydashboard", "shinythemes"), repos="https://packagemanager.rstudio.com/cran/__linux__/focal/2021-04-23")'

# allow permission
RUN sudo chown -R shiny:shiny /srv/shiny-server

# delete default example application
RUN sudo rm -rf /srv/shiny-server/sample-apps

# copy the app to the image
#COPY project.Rproj /srv/shiny-server/
COPY app.R /srv/shiny-server/
COPY README.md /srv/shiny-server/
ADD src /srv/shiny-server/src
ADD www /srv/shiny-server/www
# https://help.hcltechsw.com/domino_volt/1.0/import_sample_data.html sinon ???
ADD data /srv/shiny-server/data

## app folder
#COPY /shiny_logements_sociaux_loire_atlantique ./app
## renv.lock file
#COPY /shiny_logements_sociaux_loire_atlantique/renv.lock ./renv.lock

# install renv & restore packages
# RUN Rscript -e 'install.packages("renv")'
# ENV RENV_PATHS_CACHE="/renv/cache"
# ENV RENV_CONFIG_USE_CACHE=TRUE
# RUN Rscript -e 'renv:::renv_cache_list()'
# RUN Rscript -e 'renv::consent(provided = TRUE)'
# RUN Rscript -e 'renv::restore()'

# install R packages required 
# (change it depending on the packages needed)
# RUN R -e "install.packages('shiny', repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('shinydashboard', repos='http://cran.rstudio.com/')"
# RUN R -e "devtools::install_github('andrewsali/shinycssloaders')"
# RUN R -e "install.packages('lubridate', repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('magrittr', repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('glue', repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('DT', repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('plotly', repos='http://cran.rstudio.com/')"

#CMD ["/usr/bin/shiny-server.sh"]
#add preserve_logs true; // le cas de crash de l'appli, les logs sont accessibles via le cli dans /var/log
RUN sed -i "s|3838|8080|" /etc/shiny-server/shiny-server.conf

# expose port ???? ne sert à rien ?
EXPOSE 8080

# run app
CMD ["shiny-server"]

# run app on container start
#CMD ["R", "-e", "shiny::runApp('/srv/shiny-server', host = '0.0.0.0', port = 8080)"]