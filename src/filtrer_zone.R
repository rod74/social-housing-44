if("tidyverse" %in% rownames(installed.packages()) == FALSE) {install.packages("tidyverse")};library(tidyverse)
library(data.table)
if("sf" %in% rownames(installed.packages()) == FALSE) {install.packages("sf")};library(sf)


## @knitr sfGriddedInseeLivingStandardsOnZipcodes

## Données carroyées de l'INSEE sur les niveaux de vie en France
### [doc] (http://doc-datafoncier.cerema.fr/ff/doc_ffta/table/TUP/)
### [Source] (https://www.insee.fr/fr/statistiques/4176290?sommaire=4176305)
sfGriddedInseeLivingStandardsOnZipcodes <- function(zipcodes)
{
  griddedInseeLivingStandardsCsvPath <- 'extdata/Filosofi2015_carreaux_200m_metropole_csv/Filosofi2015_carreaux_200m_metropole.csv'
  # Ajout des colonnes nécessaires par la suite : identifiant du carreau,
  # code commune du carreau, nombre d'individus du carreau et variable indiquant 
  # si les données du carreau sont des valeurs approchées
  # listeIndic <- unique(c(listeIndic,"IdINSPIRE","Depcom","Ind","I_est_cr")) 
  
  # importation de la table des carreaux
  # et filtrage des observations selon le(s) code(s) commune(s) insee
  carreaux <- data.table::fread(griddedInseeLivingStandardsCsvPath) %>%
    dplyr::filter(Depcom %in% zipcodes) %>%
    dplyr::mutate(I_est_cr = as.integer(I_est_cr))
  # colnames(carreaux)
  # [1] "IdINSPIRE"  "Id_carr1km" "I_est_cr"   "Id_carr_n"  "Groupe"     "Depcom"     "I_pauv"     "Id_car2010" "Ind"        "Men"        "Men_pauv"  
  # [12] "Men_1ind"   "Men_5ind"   "Men_prop"   "Men_fmp"    "Ind_snv"    "Men_surf"   "Men_coll"   "Men_mais"   "Log_av45"   "Log_45_70"  "Log_70_90" 
  # [23] "Log_ap90"   "Log_inc"    "Log_soc"    "Ind_0_3"    "Ind_4_5"    "Ind_6_10"   "Ind_11_17"  "Ind_18_24"  "Ind_25_39"  "Ind_40_54"  "Ind_55_64" 
  # [34] "Ind_65_79"  "Ind_80p"    "Ind_inc"    "I_est_1km"  "x"          "y"          "geometry"
  
  # retrait des colonnes non utilisé
  # I_pauv toujours à 0 on retire
  # [1] "IdINSPIRE"  "I_est_cr", "Ind"        "Men"        "Men_pauv"  
  # [12] "Men_1ind"   "Men_5ind"   "Men_prop"   "Men_fmp"    "Ind_snv"    "Men_surf"   "Men_coll"   "Men_mais"   "Log_av45"   "Log_45_70"  "Log_70_90" 
  # [23] "Log_ap90"   "Log_inc"    "Log_soc"    "Ind_0_3"    "Ind_4_5"    "Ind_6_10"   "Ind_11_17"  "Ind_18_24"  "Ind_25_39"  "Ind_40_54"  "Ind_55_64" 
  # [34] "Ind_65_79"  "Ind_80p"    "Ind_inc"
  carreaux <- carreaux %>%
    dplyr::select("IdINSPIRE", "I_est_cr", "Ind", "Men", "Men_pauv", "Men_1ind", "Men_5ind", "Men_prop", "Men_fmp", "Ind_snv", "Men_surf", "Men_coll", "Men_mais", "Log_av45", "Log_45_70", "Log_70_90", "Log_ap90", "Log_inc", "Log_soc", "Ind_0_3", "Ind_4_5", "Ind_6_10", "Ind_11_17", "Ind_18_24", "Ind_25_39", "Ind_40_54", "Ind_55_64", "Ind_65_79", "Ind_80p", "Ind_inc")
  
  # liste des identifiants Inspire des carreaux, à partir desquels
  # on récupère leurs coordonnées, leur taille et leur code epsg
  cIdInspire <- as.character(carreaux$IdINSPIRE)
  
  epsg <- as.integer(str_sub(str_extract(carreaux[1,]$IdINSPIRE, "CRS\\d+"), 4))
  
  tailleCarreaux <- unlist(lapply(X = cIdInspire, FUN = function(ligne){
    return(as.integer(str_sub(str_extract(ligne, "RES\\d+"), 4)))
  }))
  
  ordonneesCarreaux <- unlist(lapply(X = cIdInspire, FUN = function(ligne){
    return(as.integer(str_sub(str_extract(ligne, "N\\d+"), 2)))
  }))
  
  abscissesCarreaux <- unlist(lapply(X = cIdInspire, FUN = function(ligne){
    return(as.integer(str_sub(str_extract(ligne, "E\\d+"), 2)))
  }))
  
  # ajout de 2 colonnes donnant les coordonnées du coin inférieur gauche du carreau
  carreaux$x <- abscissesCarreaux
  carreaux$y <- ordonneesCarreaux
  
  # création d'une colonne geometry contenant les coordonnées des contours des carreaux
  # puis transformation en objets geométriques a l'aide du package sf
  carreaux$geometry <- sprintf("POLYGON ((%i %i, %i %i, %i %i, %i %i, %i %i))", 
                               carreaux$x, carreaux$y, 
                               carreaux$x + tailleCarreaux, carreaux$y, 
                               carreaux$x + tailleCarreaux, carreaux$y + tailleCarreaux, 
                               carreaux$x, carreaux$y + tailleCarreaux, 
                               carreaux$x, carreaux$y) 
  
  # conversion en spatial dataframe
  carreaux_sf <- sf::st_as_sf(carreaux, wkt = "geometry", crs = epsg)
  #st_crs(carreaux_sf)
  # NOK: PROJCRS["ETRS89-extended / LAEA Europe",
  # conversion du crs sur WGS 84 pour leaflet
  carreaux_sf <- st_transform(carreaux_sf, crs = 4326)
  # st_crs(carreaux_sf)
  # OK: GEOGCRS["WGS 84""
  # glimpse(carreaux_sf)
  
  return(carreaux_sf)
}

## @knitr sfSocialHousingOnZipcodes

## RPLS - Répertoire fourni par l'Etat des constructions de logements sociaux en France en 2019
### [Source] (https://www.data.gouv.fr/fr/datasets/repertoire-des-logements-locatifs-des-bailleurs-sociaux/#_)
### [plus de détails] (http://dataviz.statistiques.developpement-durable.gouv.fr/RPLS/)
sfSocialHousingOnZipcodes <- function(zipcodes)
{

  # Filtrage sur les codes iNSEE, reformatage des longitudes/latitudes
  socialHousingFull <- fread('extdata/rpls_geoloc_2019/RPLS2019_detail_reg52.csv') %>%
    filter(DEPCOM %in% zipcodes) %>%
    mutate(longitude = as.double(str_replace(longitude, ",", "."))) %>%
    mutate(latitude = as.double(str_replace(latitude, ",", "."))) %>%
    mutate(nb_logement=1) # pour permettre un sum(n) dans le summarise du st-join avec les sfLivingStandards

  # retrait des colonnes inutiles
  # [1] "DROIT"              "DEPCOM"             "CODEPOSTAL"         "LIBCOM"             "NUMVOIE"            "INDREP"             "TYPVOIE"           
  # [8] "NOMVOIE"            "NUMAPPT"            "NUMBOITE"           "ESC"                "COULOIR"            "ETAGE"              "COMPLIDENT"        
  # [15] "ENTREE"             "BAT"                "IMMEU"              "COMPLGEO"           "LIEUDIT"            "QPV"                "TYPECONST"         
  # [22] "NBPIECE"            "SURFHAB"            "CONSTRUCT"          "LOCAT"              "PATRIMOINE"         "ORIGINE"            "FINAN"             
  # [29] "FINANAUTRE"         "CONV"               "NUMCONV"            "DATCONV"            "NEWLOGT"            "CUS"                "DPEDATE"           
  # [36] "DPEENERGIE"         "DPESERRE"           "SRU_EXPIR"          "SRU_ALINEA"         "CODSEGPATRIM"       "LIBSEGPATRIM"       "PMR"               
  # [43] "REG"                "LIBREG"             "DEP"                "LIBDEP"             "EPCI"               "LIBEPCI"            "EPSG"              
  # [50] "X"                  "Y"                  "plg_qp"             "plg_iris"           "plg_zus"            "plg_zfu"            "plg_qva"           
  # [57] "QUALITE_VOIE"       "QUALITE_NUMERO"     "QUALITE_XY"         "DISTANCE_PRECISION" "QUALITE_QP"         "QUALITE_IRIS"       "QUALITE_ZUS"       
  # [64] "QUALITE_ZFU"        "QUALITE_QVA"        "COMAQP"             "COMRIL"             "QUALITE_SDES"       "result_label"       "result_type"       
  # [71] "result_id"          "result_score"       "geometry"
  
  # on peut garder à voir
  # "DROIT", "QPV", "TYPECONST", "NBPIECE", "SURFHAB", "CONSTRUCT", "LOCAT", "PATRIMOINE", "ORIGINE", "FINAN", "FINANAUTRE", "CONV", "DATCONV", "NEWLOGT",
  # "CUS", "DPEDATE", "DPEENERGIE", "DPESERRE", "SRU_EXPIR", "SRU_ALINEA", "CODSEGPATRIM", "LIBSEGPATRIM", "PMR", "REG", "LIBREG", "DEP", "LIBDEP",
  # "EPCI", "LIBEPCI", "QUALITE_SDES", "result_score", "geometry"
  socialHousing <- socialHousingFull %>%
    select("QPV", "TYPECONST", "NBPIECE", "SURFHAB", "longitude", "latitude", "nb_logement") %>%
    mutate(individuel = ifelse(TYPECONST == "I", 1, 0)) %>%
    mutate(collectif = ifelse(TYPECONST == "C", 1, 0)) %>%
    mutate(etudiant = ifelse(TYPECONST == "E", 1, 0)) %>%
    mutate(qpv = ifelse(QPV == 1, 1, 0))
  
  # glimpse(socialHousing)
  
  # Conversion en spatial dataframe avec le crs WGS 84 pour leaflet
  sfSocialHousing <- socialHousing %>%
    filter(!is.na(longitude), !is.na(latitude)) %>%
    sf::st_as_sf(coords = c("longitude", "latitude"), crs = 4326)
  #st_crs(sfSocialHousing)
  # OK: GEOGCRS["WGS 84",
  
  return(sfSocialHousing)
}

## @knitr sfLivingStandardsWithSocialHousing

# spatial join between living standards and social housing
sfLivingStandardsWithSocialHousing <- function(zipcodes)
{

  # living standards
  sfLivingStandards <- sfGriddedInseeLivingStandardsOnZipcodes(zipcodes)  
  
  # social housing
  sfSocialHousing <- sfSocialHousingOnZipcodes(zipcodes)
  
  # Jointure spatiale des logements sociaux sur les zones de revenus
  # on a des points (logements sociaux) qu'on veut lier à des polygones (carreau 200m)
  sfData <- sf::st_join(sfLivingStandards, sfSocialHousing)
  
  # Indicateurs des logements par carreau
  sfSocialHousingByCarroi200 <- sfData %>%
    group_by(IdINSPIRE, Ind, Men, Men_pauv, Men_1ind, Men_5ind, Men_prop, Men_fmp, Men_surf, Men_coll, Men_mais, Ind_snv, Ind_0_3, Ind_11_17, Ind_4_5, Ind_6_10, Ind_18_24, Ind_25_39, Ind_40_54, Ind_55_64, Ind_65_79, Ind_80p, Ind_inc) %>%
    summarise_at(vars(individuel, collectif, etudiant, qpv, SURFHAB, NBPIECE, nb_logement), funs(sum))
  
  label_interval <- function(breaks) {
    paste0(round(breaks[1:length(breaks) - 1]/1000, 1), " K&euro;", " - ", round(breaks[2:length(breaks)]/1000, 1), " K&euro;")
  }
  sfSocialHousingByCarroi200 <- sfSocialHousingByCarroi200 %>%
    mutate(Ind_snv_medium = trunc(Ind_snv/Ind))
  
  seuilsRevenu <- getBreaks(sfSocialHousingByCarroi200$Ind_snv_medium, nclass=5, method = "fisher-jenks")
  
  sfSocialHousingByCarroi200 <- sfSocialHousingByCarroi200 %>%
    mutate(Ind_snv_class = cut(Ind_snv_medium, breaks = seuilsRevenu, labels = label_interval(seuilsRevenu))) %>%
    mutate(PourCentPauvre=Men_pauv/Men) %>%
    mutate(longitude=st_coordinates(st_centroid(geometry))[1]) %>%
    mutate(latitude=st_coordinates(st_centroid(geometry))[2])
  
  return(sfSocialHousingByCarroi200)
}
